import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';
import { DrivingController } from './driving/driving.controller';
import { GoogleMapDirective } from './common/googleMap/google.map.directive';
import { GoogleMapService } from './common/googleMap/google.map.service';
import { LocalStorageService } from './common/storage/local.storage.service';

angular.module('deployDrivingRoutes', ['ngMessages', 'ui.router', 'ui.bootstrap', 'ngStorage'])
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .directive('googleMap', GoogleMapDirective)
  .service('googleMapService', GoogleMapService)
  .service('localStorageService', LocalStorageService)
  .controller('DrivingController', DrivingController)
  .controller('MainController', MainController);
