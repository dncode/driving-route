export class DrivingController {
  constructor ($log, $state, $stateParams, localStorageService) {
    'ngInject';

    this.$log = $log;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.routeIndex = this.$stateParams.index;
    this.localStorageService = localStorageService;
    this.route = this.localStorageService.getDataByIndex('routes', parseInt(this.$stateParams.index) - 1);

    this.activate();
  }

  activate() {
    if(!this.$stateParams.index) {
      this.$state.go('home');
    }
  }
}
