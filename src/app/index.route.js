export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'vm'
    }).state('map', {
      url: '/driving-route/:index',
      templateUrl: 'app/driving/driving.map.html',
      controller: 'DrivingController',
      controllerAs: 'vm'
    });

  $urlRouterProvider.otherwise('/');
}
