export class LocalStorageService {
  constructor ($log, $localStorage) {
    'ngInject';

    this.$log = $log;
    this.$localStorage = $localStorage;
  }

  getData(namespace) {
    return this.$localStorage[namespace] && this.$localStorage[namespace].length > 0 ? this.$localStorage[namespace] : null;
  }

  saveData(namespace, data) {
    this.$localStorage[namespace] = data;
    return this.getData(namespace);
  }
  
  getDataByIndex(namespace, index) {
   return this.$localStorage[namespace] ? this.$localStorage[namespace][index] : null;
  }
}
