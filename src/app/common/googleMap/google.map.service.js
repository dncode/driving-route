export class GoogleMapService {
  constructor ($log, $q) {
    'ngInject';

    this.$log = $log;
    this.$q = $q;

    this.directionsDisplay = null;
    this.directionsService = new google.maps.DirectionsService();
    this.routeRequest = {
      origin: '',
      destination: '',
      travelMode: google.maps.DirectionsTravelMode.DRIVING
    };

    this.allRoutes = [];
  }

  getRoute(start, end) {
    let deferred = this.$q.defer();
    this.routeRequest.origin = start,
    this.routeRequest.destination = end;

    this.directionsService.route(this.routeRequest, function (response, status) {
      if (status === google.maps.DirectionsStatus.OK) {
        deferred.resolve(response);
      } else {
        deferred.reject(status);
      }
    });

    return deferred.promise;
  }

  displayRoute(route, map) {
    this.directionsDisplay = new google.maps.DirectionsRenderer({ map });
    this.directionsDisplay.setDirections(route);
  }
}
