export function GoogleMapDirective() {
  'ngInject';

  let directive = {
    restrict: 'E',
    scope: {
        route: '='
    },
    templateUrl: 'app/common/googleMap/google.map.html',
    controller: GoogleMapController,
    controllerAs: 'vm'
  };

  return directive;
}

class GoogleMapController {
  constructor($scope, $document, googleMapService) {
    'ngInject';
  
    this.$scope = $scope;
    this.map = new google.maps.Map($document[0].getElementById('gMap'), {
      zoom: 7
    });
    this.route = $scope.route;
    this.googleMapService = googleMapService;

    this.activate();
  }

  activate() {
    this.googleMapService.displayRoute(this.route, this.map);
  }
  
}
