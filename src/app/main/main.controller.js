export class MainController {
  constructor ($log, $window, $timeout, $state, localStorageService, googleMapService) {
    'ngInject';

    this.$log = $log;
    this.$window = $window;
    this.$timeout = $timeout;
    this.$state = $state;
    this.localStorageService = localStorageService;
    this.googleMapService = googleMapService;

    this.route = {};
    this.routes = this.localStorageService.getData('routes') || [];
    this.alertConfig = {
      type: '',
      msg: '',
      show: false
    }
  }

  deleteRoute(route) {
    this.routes.splice(this.routes.indexOf(route), 1);
    this.routes = this.localStorageService.saveData('routes', this.routes);
  }

  submit() {    
    let vm = this;

    if(this._checkRoute()) {
      this.googleMapService.getRoute(this.route.start, this.route.end).then((route) => {
        route.start = this.route.start;
        route.end = this.route.end;
        vm.routes.push(route);
        vm.localStorageService.saveData('routes', vm.routes);
        vm.$state.go('map', {index: vm.routes.length});
      }).catch(() => {
        this.showAlert('alert-danger', 'Invalid route!');
      });
    } else {
      this.showAlert('alert-warning', 'Route already exist!');
    }
  }

  _checkRoute() {
    let exist = true;

    angular.forEach(this.routes, (value) => {
      if(value.start.toLowerCase() === this.route.start.toLowerCase() && value.end.toLowerCase() === this.route.end.toLowerCase()) {
        exist = false;
        return;
      }
    });

    return exist;
  }

  showAlert(type, msg) {
    this.alertConfig.show = true;
    this.alertConfig.msg = msg;
    this.alertConfig.type = type;

    this.$timeout(() => {
      this.alertConfig.show = false;
    }, 2500);
  }
}
